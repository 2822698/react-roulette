import * as React from "react";
import { Sector } from "@/interfaces/game";
import { randomNumber } from "@/utils";
import "./roulette.scss";

interface PropsInterface {
  sectorMap: Sector[];
  targetSectorId: number;
  onStartOfAnimation?: () => void;
  onEndOfAnimation?: () => void;
}

interface StateInterface {
  dur: number;
  startDegree: number;
  currentDegree: number;
  targetDegree: number;
  n: number;
  rouletteRotation: boolean;
}

export default class Roulette extends React.Component<
  PropsInterface,
  StateInterface
> {
  public state: StateInterface = {
    dur: 6000,
    startDegree: 0,
    currentDegree: 0,
    targetDegree: 0,
    n: 4,
    rouletteRotation: false
  };

  /**
   * Метод вычисляет угол в интервале допустимых углов (от начала до конца целвеого сектора)
   */
  public calculateTargetDegree() {
    const { n, startDegree } = this.state;
    const { sectorMap } = this.props;
    const sectorIndex = sectorMap.findIndex(
      ({ id }) => id === this.props.targetSectorId
    );
    if (sectorIndex) {
      const sector = sectorMap[sectorIndex];
      const prevSector = sectorIndex ? sectorMap[sectorIndex - 1] : null;
      const degree = prevSector
        ? randomNumber(sector.offset, prevSector.offset)
        : randomNumber(0, sector.offset);
      return sector
        ? 360 * n + (degree - (startDegree % 360)) + startDegree
        : 0;
    }
  }

  // начальная угловая скорость
  get startAngularSpeed() {
    const { n, dur, startDegree, targetDegree } = this.state;
    return (2 * (targetDegree - startDegree)) / dur;
  }

  // угловое ускорение
  get angularAcceleration() {
    return (-1 * this.startAngularSpeed) / this.state.dur;
  }

  get rotationStyle() {
    return {
      transform: `rotate(${this.state.currentDegree}deg) translateZ(0)`
    };
  }

  /**
   * Вычисляет текущий граус исходя из уравнения равнозамедленного движения
   * @param time
   */
  public updateDegree(time: number) {
    const currentDegree =
      (this.angularAcceleration * time * time) / 2 +
      this.startAngularSpeed * time +
      this.state.startDegree;

    this.setState({
      currentDegree
    });
  }

  public endOfAnimation() {
    this.setState({
      startDegree: this.state.currentDegree,
      rouletteRotation: false
    });
    if (this.props.onEndOfAnimation) {
      this.props.onEndOfAnimation();
    }
  }

  // вращаем
  public startRotation = () => () => {
    if (this.state.rouletteRotation) return;
    if (this.props.onStartOfAnimation) {
      this.props.onStartOfAnimation();
    }
    this.setState({
      targetDegree: this.calculateTargetDegree() || 0,
      rouletteRotation: true
    });
    const startTime = performance.now();

    const animate = (now: number) => {
      let time = now - startTime;
      time = time < 0 ? 0 : time;

      if (time > this.state.dur) time = this.state.dur;

      this.updateDegree(time);

      if (time < this.state.dur) requestAnimationFrame(animate);
      else this.endOfAnimation();
    };

    requestAnimationFrame(animate);
  };

  render() {
    return (
      <div className="roulette">
        <div className="roulette__ligth-ring" />
        <div className="roulette__bg" />
        <div className="roulette__items" style={this.rotationStyle} />
        <div className="roulette__light" />
        <div className="roulette__spinner">
          <div
            onClick={this.startRotation()}
            className={
              !this.state.rouletteRotation
                ? "roulette__spinner-label roulette__spinner-label--animate"
                : "roulette__spinner-label"
            }
          />
        </div>
      </div>
    );
  }
}
