import * as React from "react";
import Roulette from "@/components/Roulette/Roulette";
import GameResults from "@/components/GameResults/GameResults";
import { CSSTransitionGroup } from "react-transition-group";
import { Sector } from "@/interfaces/game";
import "./game.scss";

interface StateInterface {
  gameResultsDialogShown: boolean;
}

interface PropsInterface {
  gameLoaded: boolean;
  isFetching: boolean;
  sectors?: Sector[];
  targetSectorId?: number;
  getGame: () => void;
  setGameStatus: (status: boolean) => void;
}

export default class Game extends React.Component<
  PropsInterface,
  StateInterface
> {
  public state: StateInterface = {
    gameResultsDialogShown: false
  };

  get targetSector() {
    const { targetSectorId, sectors } = this.props;
    return targetSectorId && sectors
      ? sectors.find(sector => sector.id === targetSectorId)
      : undefined;
  }

  public showGameResultsDialog = () => () => {
    this.setState({ gameResultsDialogShown: true });
  };

  public closeResultsAndClearGame = () => () => {
    this.setState({ gameResultsDialogShown: false });
    this.props.setGameStatus(false);
    this.props.getGame();
  };

  render() {
    const rouletteTransition = {
      transitionName: "vertical-slide",
      transitionEnterTimeout: 300,
      transitionLeave: false
    };

    const gameResultsTransition = {
      transitionName: "fade",
      transitionLeave: false,
      transitionEnter: true,
      transitionEnterTimeout: 300
    };

    return (
      <div className="game">
        <CSSTransitionGroup {...gameResultsTransition}>
          {this.state.gameResultsDialogShown && this.targetSector && (
            <GameResults
              key="gameResults"
              onClickCloseBtn={this.closeResultsAndClearGame()}
              targetSector={this.targetSector}
            />
          )}
        </CSSTransitionGroup>
        <CSSTransitionGroup {...rouletteTransition}>
          {this.props.gameLoaded &&
          this.props.sectors &&
          this.props.targetSectorId ? (
            <div className="game__wrapper" key="roulette">
              <div className="game__game-area">
                <h1 className="game__header">Spin & Win</h1>
                <div className="game__subheader">Tap "Spin" to Play</div>
                <Roulette
                  onEndOfAnimation={this.showGameResultsDialog()}
                  sectorMap={this.props.sectors}
                  targetSectorId={this.props.targetSectorId}
                />
              </div>
            </div>
          ) : (
            <div className="before-game" key="btn">
              <div
                className="before-game__start-btn"
                onClick={this.props.getGame}
              >
                <img
                  src={require("./assets/roulette__ico.svg")}
                  className="before-game__start-btn-icon"
                />
                <div className="before-game__start-btn-text">
                  {this.props.isFetching ? (
                    <span className="loading">Loading</span>
                  ) : (
                    "Spin & Win"
                  )}
                </div>
              </div>
            </div>
          )}
        </CSSTransitionGroup>
      </div>
    );
  }
}
