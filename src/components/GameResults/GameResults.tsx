import * as React from "react";
import { Sector } from "@/interfaces/game";
import "./game-results.scss";
import { Link } from "react-router-dom";

interface PropsInterface {
  targetSector: Sector;
  onClickCloseBtn?: () => void;
}

export default function gameResults(props: PropsInterface) {
  const { targetSector, onClickCloseBtn } = props;
  return (
    <div className="game-results">
      <div className="game-results__close-btn" onClick={onClickCloseBtn}>
        x
      </div>
      <div className="game-results__results">
        <div className="game-results__points">
          <div>You won</div>
          <div className="game-results__points-count">{targetSector.count}</div>
          <div>Fuel Points</div>
        </div>
        <Link to="/shop">
          <button type="button" className="btn">
            Go Shop
          </button>
        </Link>
      </div>
    </div>
  );
}
