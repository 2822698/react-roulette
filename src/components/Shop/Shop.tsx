import * as React from "react";
import { Category, ChildCategory, Product } from "@/interfaces/shop";
import Categories from "./Categories/Categories";
import Catalog from "./Catalog/Catalog";
import "./shop.scss";

interface StateInterface {
  selectedCategory?: Category | ChildCategory;
}

interface PropsInterface {
  categories: {
    items: Category[];
    isFetching: boolean;
    error?: { status: number };
  };
  products: {
    items: Product[];
    isFetching: boolean;
    error?: { status: number };
    page?: number;
    isLastPage?: boolean;
  };
  getCategories: () => any;
  getProducts: (categoryId: number, page: number) => any;
}

export default class Shop extends React.Component<
  PropsInterface,
  StateInterface
> {
  public state: StateInterface = {};

  public componentDidMount() {
    this.props.getCategories();
  }

  public selectCategory = () => (category: Category | ChildCategory) => {
    this.setState({ selectedCategory: category });
    this.props.getProducts(category.id, 0);
  };

  public getMoreProducts = () => () => {
    const page =
      typeof this.props.products.page === "number"
        ? this.props.products.page + 1
        : 0;
    if (this.state.selectedCategory)
      this.props.getProducts(this.state.selectedCategory.id, page);
  };

  render() {
    const { categories, products } = this.props;
    const { selectedCategory } = this.state;
    return (
      <div className="shop">
        <Categories
          categories={categories.items}
          selectedCategory={selectedCategory}
          selectCategory={this.selectCategory()}
        />
        <Catalog
          getMoreProducts={this.getMoreProducts()}
          selectedCategory={selectedCategory}
          isLastPage={products.isLastPage}
          products={products.items}
          page={products.page}
          isFetching={products.isFetching}
        />
      </div>
    );
  }
}
