import * as React from "react";
import { Product } from "@/interfaces/shop";
import "./catalog-item.scss";

interface PropsInterface {
  product: Product;
}

export default function catalogItem({ product }: PropsInterface) {
  return (
    <div className="catalog-item">
      <div
        className="catalog-item__preview"
        style={{
          backgroundImage: `url(${product.image_url})`
        }}
      />
      <div className="catalog-item__title">{product.name}</div>
      <div className="catalog-item__price">
        {product.lowest_price} {product.currency}
      </div>
    </div>
  );
}
