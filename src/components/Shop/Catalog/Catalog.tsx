import * as React from "react";
import { Product, Category, ChildCategory } from "@/interfaces/shop";
import CatalogItem from "./CatalogItem/CatalogItem";
import "./catalog.scss";

interface PropsInterface {
  selectedCategory?: Category | ChildCategory;
  getMoreProducts: () => any;
  products: Product[];
  page?: number;
  isLastPage?: boolean;
  isFetching?: boolean;
}

interface StateInterface {}

export default class Catalog extends React.Component<
  PropsInterface,
  StateInterface
> {
  get categoryName() {
    const { selectedCategory } = this.props;
    if (!selectedCategory) return "";
    return "clean_name" in selectedCategory
      ? selectedCategory.clean_name
      : selectedCategory.name;
  }

  public render() {
    const {
      isFetching,
      selectedCategory,
      products,
      isLastPage,
      getMoreProducts
    } = this.props;

    return (
      <div className="catalog">
        {selectedCategory && (
          <div className="catalog__category">
            Products in category: {this.categoryName}
          </div>
        )}
        <div className="catalog__items-wrapper">
          {products.map(product => (
            <div className="catalog__item" key={product.id}>
              <CatalogItem product={product} />
            </div>
          ))}
        </div>
        {selectedCategory && !isLastPage && (
          <div className="catalog__show-more-btn-wrapper">
            <button className="btn" onClick={getMoreProducts}>
              Show More
            </button>
          </div>
        )}
        {isFetching && (
          <div className="catalog__loader-wrapper">
            <div className="catalog__loader" />
          </div>
        )}
      </div>
    );
  }
}
