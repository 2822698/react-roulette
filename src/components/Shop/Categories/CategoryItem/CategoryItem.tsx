import * as React from "react";
import { Category, ChildCategory } from "@/interfaces/shop";
import "./categories-item.scss";

interface PropsInterface {
  category: Category;
  selectedCategory?: Category | ChildCategory;
  onSelect: (cat: Category | ChildCategory) => any;
}

export default function categoryItem(props: PropsInterface) {
  const { category, selectedCategory, onSelect } = props;
  const itemId = `category${category.innerId}`;

  return (
    <div
      className={
        category === selectedCategory
          ? "categories-item categories-item--active"
          : "categories-item"
      }
    >
      <label
        className="categories-item__label"
        htmlFor={itemId}
        onClick={() => category.id && onSelect(category)}
      >
        {category.clean_name}
      </label>
      <input
        className="categories-item__checkbox"
        type="checkbox"
        id={itemId}
        hidden
      />
      {category.children && (
        <div className="categories-item__child-list">
          {category.children.map(child => (
            <div
              key={`category${child.innerId}`}
              className={
                child === selectedCategory
                  ? "categories-item categories-item--active"
                  : "categories-item"
              }
              onClick={() => child.id && onSelect(child)}
            >
              {child.name}
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
