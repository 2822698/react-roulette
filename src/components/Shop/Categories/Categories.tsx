import * as React from "react";
import { Category, ChildCategory } from "@/interfaces/shop";
import CategoryItem from "./CategoryItem/CategoryItem";
import "./categories.scss";

interface PropsInterface {
  categories: Category[];
  selectedCategory?: Category | ChildCategory;
  selectCategory: (category: Category | ChildCategory) => void;
}

export default function categories(props: PropsInterface) {
  const { categories, selectedCategory, selectCategory } = props;

  return (
    <div className="categories">
      <div className="categories__title">Select Category</div>
      <div className="categories__select-category">
        <div className="categories__list">
          {categories.map(category => (
            <CategoryItem
              key={category.innerId}
              selectedCategory={selectedCategory}
              onSelect={selectCategory}
              category={category}
            />
          ))}
        </div>
      </div>
    </div>
  );
}
