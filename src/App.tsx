import * as React from "react";
import Routes from "./Routes";
import "./app.scss";

const App = () => (
  <div className="app">
    <Routes />
  </div>
);

export default App;
