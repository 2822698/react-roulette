export interface ChildCategory {
  id: number;
  innerId: number;
  name: string;
}

export interface Category {
  id: number;
  innerId: number;
  name: string;
  clean_name: string;
  description: string;
  view: string;
  is_all_tab: boolean;
  name_color?: string;
  description_color?: string;
  background_color?: string;
  image_url: string;
  background_url?: string;
  short_name: string;
  children?: ChildCategory[];
}

export interface Product {
  id: number;
  merchant_id: number;
  name: string;
  modifications_count: number;
  modification_id: number;
  lowest_price: string;
  discount: string;
  image_url: string;
  country: string;
  rating: string;
  reviews_count: number;
  currency: string;
}
