export interface Sector {
  id: number;
  count: number;
  offset: number;
}
