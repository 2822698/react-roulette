import * as React from "react";
import { Switch, Route } from "react-router-dom";
import GameContainer from "@/containers/GameContainer";
import ShopContainer from "@/containers/ShopContainer";

const Main = () => (
  <main className="main">
    <Switch>
      <Route exact path="/" component={GameContainer} />
      <Route path="/shop" component={ShopContainer} />
    </Switch>
  </main>
);

export default Main;
