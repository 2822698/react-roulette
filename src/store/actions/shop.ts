import {
  FetchCategoriesResponse,
  fetchCategories,
  fetchProducts,
  FechProductsResponse
} from "@/api/shop";
import { Category, Product } from "@/interfaces/shop";

export const FETCH_CATEGORIES_REQUEST = "FETCH_CATEGORIES";
export const FETCH_CATEGORIES_SUCCESS = "FETCH_CATEGORIES_SUCCESS";
export const FETCH_CATEGORIES_ERROR = "FETCH_CATEGORIES_ERROR";
export const FETCH_PRODUCTS_REQUEST = "FETCH_PRODUCTS_REQUEST";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_ERROR = "FETCH_PRODUCTS_ERROR";

export const fetchCategoriesRequest = () => ({
  type: FETCH_CATEGORIES_REQUEST
});

export const fetchCategoriesSuccess = (categories: Category[]) => ({
  categories,
  type: FETCH_CATEGORIES_SUCCESS
});

export const fetchCategoriesError = (errStatus: number) => ({
  status: errStatus,
  type: FETCH_CATEGORIES_ERROR
});

export const loadCategories = () => dispatch => {
  dispatch(fetchCategoriesRequest);
  fetchCategories()
    .then((response: FetchCategoriesResponse) => {
      dispatch(fetchCategoriesSuccess(response.collection));
    })
    .catch(err => {
      dispatch(fetchCategoriesError(err.status));
    });
};

export const fetchProductsRequest = () => ({
  type: FETCH_PRODUCTS_REQUEST
});

export const fetchProductsSuccess = (
  products: Product[],
  page: number,
  isLastPage: boolean
) => ({
  products,
  page,
  isLastPage,
  type: FETCH_PRODUCTS_SUCCESS
});

export const fetchProductsError = (errStatus: number) => ({
  status: errStatus,
  type: FETCH_PRODUCTS_ERROR
});

export const loadProducts = (categoryId: number, page: number) => dispatch => {
  dispatch(fetchProductsRequest());

  fetchProducts(categoryId, page)
    .then((response: FechProductsResponse) => {
      dispatch(
        fetchProductsSuccess(
          response.collection,
          response.page,
          response.is_last_page
        )
      );
    })
    .catch(err => {
      dispatch(fetchProductsError(err.status));
    });
};
