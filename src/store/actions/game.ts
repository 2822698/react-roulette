import { Sector } from "@/interfaces/game";
import { fetchGame, FetchGameResponse } from "@/api/game";

export const FETCH_GAME_REQUEST = "FETCH_GAME_REQUEST";
export const FETCH_GAME_SUCCESS = "FETCH_GAME_SUCCESS";
export const SET_GAME_STATUS = "SET_GAME_STATUS";

export const setGameStatus = (status: boolean) => ({
  status,
  type: SET_GAME_STATUS
});

export const fetchGameRequest = () => ({
  type: FETCH_GAME_REQUEST
});

export const fetchGameSuccess = (response: FetchGameResponse) => ({
  ...response,
  type: FETCH_GAME_SUCCESS
});

export const getGame = () => dispatch => {
  dispatch(fetchGameRequest());
  fetchGame().then((response: FetchGameResponse) =>
    dispatch(fetchGameSuccess(response))
  );
};
