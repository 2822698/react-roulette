import { combineReducers } from "redux";
import game from "./game";
import shop from "./shop";

const rootReducer = combineReducers({
  shop,
  game
});

export default rootReducer;
