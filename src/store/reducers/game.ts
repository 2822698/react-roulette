import { fromJS } from "immutable";
import { Sector } from "@/interfaces/game";
import {
  FETCH_GAME_REQUEST,
  FETCH_GAME_SUCCESS,
  SET_GAME_STATUS
} from "@/store/actions/game";

export interface GameState {
  sectors: Sector[];
  targetSectorId: number;
  gameLoaded: boolean;
  isFetching: boolean;
}

export const initialState = fromJS({
  gameLoaded: false,
  isFetching: false,
  sectors: null,
  targetSectorId: null
});

export default function game(state = initialState, action) {
  switch (action.type) {
    case SET_GAME_STATUS:
      return state.set("gameLoaded", action.status);
    case FETCH_GAME_REQUEST:
      return state.set("isFetching", true).set("gameLoaded", false);
    case FETCH_GAME_SUCCESS:
      return state
        .set("isFetching", false)
        .set("gameLoaded", true)
        .set("targetSectorId", action.targetSectorId)
        .set("sectors", action.sectors);
    default:
      return state;
  }
}
