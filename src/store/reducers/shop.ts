import { fromJS } from "immutable";
import { Category, Product } from "@/interfaces/shop";
import { randomString } from "@/utils";
import {
  FETCH_CATEGORIES_REQUEST,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_ERROR,
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_ERROR
} from "@/store/actions/shop";

export interface ShopState {
  categories: {
    items: Category[];
    isFetching: boolean;
    error?: { status: number };
  };
  products: {
    items: Product[];
    isFetching: boolean;
    error?: { status: number };
    page: number;
    isLastPage: boolean;
  };
}

export const initialState = fromJS({
  categories: {
    isFetching: false,
    items: []
  },
  products: {
    items: [],
    isFetching: false
  }
});

// так как API отдает нам категории, у некоторых из которых (те, которые имеют потомков)
// отсутствуют id, генерируем каждой категории внутренний id для нужд клиента
const generateInnerIds = (categories: Partial<Category>[]) => {
  let ids: string[];
  ids = [];
  const genetateRandom = () => {
    let id = randomString(30);
    while (ids.includes(id)) {
      id = randomString(30);
    }
    return id;
  };
  return categories.map(category => ({
    ...category,
    innerId: genetateRandom(),
    children: category.children
      ? category.children.map(children => ({
          ...children,
          innerId: genetateRandom()
        }))
      : undefined
  }));
};

export default function shop(state = initialState, action) {
  switch (action.type) {
    case FETCH_CATEGORIES_REQUEST:
      return state.setIn(["categories", "isFetching"], true);
    case FETCH_CATEGORIES_SUCCESS:
      return state
        .setIn(["categories", "isFetching"], false)
        .setIn(["categories", "items"], generateInnerIds(action.categories));
    case FETCH_CATEGORIES_ERROR:
      return state.setIn(["categories", "error"], { status: action.status });
    case FETCH_PRODUCTS_REQUEST:
      return state.setIn(["products", "isFetching"], true);
    case FETCH_PRODUCTS_SUCCESS:
      const newState = state
        .setIn(["products", "isFetching"], false)
        .setIn(["products", "page"], action.page)
        .setIn(["products", "isLastPage"], action.isLastPage);
      if (action.page > 0) {
        return newState.updateIn(["products", "items"], items =>
          items.concat(action.products)
        );
      } else {
        return newState.setIn(["products", "items"], action.products);
      }
    default:
      return state;
  }
}
