import reduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import { GameState } from './reducers/game';

export interface StateInterface {
  game: GameState;
}

export const store = createStore(reducers, applyMiddleware(reduxThunk));
