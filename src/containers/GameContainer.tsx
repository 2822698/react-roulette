import { connect } from "react-redux";
import { getGame, setGameStatus } from "@/store/actions/game";
import Game from "@/components/Game/Game";

const mapStateToProps = state => {
  return {
    gameLoaded: state.game.get("gameLoaded"),
    isFetching: state.game.get("isFetching"),
    sectors: state.game.get("sectors"),
    targetSectorId: state.game.get("targetSectorId")
  };
};

const mapDispatchToProps = dispatch => ({
  getGame: () => dispatch(getGame()),
  setGameStatus: (status: boolean) => dispatch(setGameStatus(status))
});

const GameContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);

export default GameContainer;
