import { connect } from "react-redux";
import Shop from "@/components/Shop/Shop";
import { loadCategories, loadProducts } from "@/store/actions/shop";

// я бы мог неявно получить из immutable инстанса
// все хранилище сразу и забиндить его в одну строку
// но в данном случае, при привязке пропсов контейнера
// я считаю целесообразным явно забиндить структуру пропсов
const mapStateToProps = state => {
  return {
    categories: {
      isFetching: state.shop.getIn(["categories", "isFetching"]),
      items: state.shop.getIn(["categories", "items"])
    },
    products: {
      isFetching: state.shop.getIn(["products", "isFetching"]),
      items: state.shop.getIn(["products", "items"]),
      page: state.shop.getIn(["products", "page"]),
      isLastPage: state.shop.getIn(["products", "isLastPage"])
    }
  };
};

const mapDispatchToProps = dispatch => ({
  getCategories: () => dispatch(loadCategories()),
  getProducts: (categoryId: number, page: number) =>
    dispatch(loadProducts(categoryId, page))
});

const ShopContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Shop);

export default ShopContainer;
