import axios from "axios";
import { Category, Product } from "@/interfaces/shop";

const fetch = axios.create({
  baseURL: "/api/v2/"
});

export interface FetchCategoriesResponse {
  collection: Category[];
}

export async function fetchCategories(): Promise<FetchCategoriesResponse> {
  const { data } = await fetch.get("/categories");
  return data;
}

export interface FechProductsResponse {
  collection: Product[];
  page: number;
  per_page: number;
  is_last_page: boolean;
}

export async function fetchProducts(
  categoryId: number,
  page?: number
): Promise<FechProductsResponse> {
  const { data } = await fetch.get(`/categories/${categoryId}/products`, {
    params: {
      page: page || 0
    }
  });
  return data;
}
