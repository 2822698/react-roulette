import { Sector } from "@/interfaces/game";
import { sectors, getRandomSector } from "./fakeData";

export interface FetchGameResponse {
  sectors: Sector[];
  targetSectorId: number;
}

export function fetchGame(): Promise<FetchGameResponse> {
  // симуляируем ассинхронность, как будто бы
  // мы получаем данные об игре с сервера
  return new Promise((resolve, reject) => {
    setTimeout(
      () => resolve({ sectors, targetSectorId: getRandomSector() }),
      1500
    );
  });
}
