import { Sector } from "@/interfaces/game";
import { randomNumber } from "@/utils";
/*
  Конфигурация колеса фортуны.
  В полноценном приложении должна прилетать с бэкенда
  В нашем же демо приложении мы просто задаем ее прямо здесь
*/
const data = [
  {
    count: 100,
    offset: 24
  },
  {
    count: 500,
    offset: 52
  },
  {
    count: 50,
    offset: 80
  },
  {
    count: 2000,
    offset: 104
  },
  {
    count: 1000,
    offset: 131
  },
  {
    count: 25,
    offset: 153
  },
  {
    count: 250,
    offset: 180
  },
  {
    count: 1500,
    offset: 201
  },
  {
    count: 750,
    offset: 225
  },
  {
    count: 75,
    offset: 249
  },
  {
    count: 5000,
    offset: 270
  },
  {
    count: 350,
    offset: 292
  },
  {
    count: 2500,
    offset: 317
  },
  {
    count: 150,
    offset: 340
  },
  {
    count: 15000,
    offset: 360
  }
];

export const sectors: Sector[] = data.map((sector, id) =>
  Object.assign(sector, {
    id
  })
);

export const getRandomSector = () => randomNumber(0, sectors.length - 1);
